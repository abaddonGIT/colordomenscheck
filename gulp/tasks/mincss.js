/**
 * Created by abaddon on 27.01.2015.
 */
var gulp = require('gulp'),
    mincss = require('gulp-minify-css'),
    size = require('gulp-filesize'),
    del = require('del'),
    rename = require('gulp-rename'),
    autoprefixer = require('gulp-autoprefixer'),
    config = require('../config').cssmin;

gulp.task('mincss', ['prefix'], function () {
    //del(config.dest + 'style.min.css');
    return gulp.src(config.src)
        .pipe(mincss({keepBreaks: true}))
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest(config.dest))
        .pipe(size());
});