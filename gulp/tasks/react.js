/**
 * Created by abaddon on 03.02.2015.
 */
var gulp = require('gulp'),
    uglify = require('gulp-react'),
    size = require('gulp-filesize'),
    config = require('../config').react;

gulp.task('react', function () {
    return gulp.src(config.src)
        .pipe(react())
        .pipe(gulp.dest(config.dest))
        .pipe(size());
});