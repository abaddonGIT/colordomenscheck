/**
 * Created by abaddon on 18.12.2014.
 */
(function (require) {
    "use strict";
    var gulp = require('gulp'),
        browserify = require('browserify'),
        reactify = require('reactify'),
        source = require('vinyl-source-stream'),
        zip = require('gulp-zip'),
        size = require('gulp-filesize'),
        zip = require('gulp-zip'),
        uglify = require('gulp-uglify');

    var requireDir = require('require-dir');
    requireDir('./gulp/tasks', {recurse: true});

    var pages = ['options', 'popup', 'bg'];

    pages.forEach(function (name) {
        gulp.task(name + 'React', function () {
            var b = browserify();
            if (name !== 'bg') {
                b.transform(reactify);
            }
            b.add('./src/js/' + name + '.js');
            return b.bundle()
                .pipe(source(name + '.compile.js'))
                .pipe(gulp.dest('./src/js'));
        });
        gulp.task(name, [name + 'React'], function () {
            return gulp.src('./src/js/' + name + '.compile.js')
                .pipe(uglify())
                .pipe(gulp.dest('./src/js/min'))
                .pipe(size());
        });
    });

    gulp.task('watch:all', function () {
        gulp.watch('src/js/components/*.jsx', ['popup']);
        gulp.watch('src/js/components/*.jsx', ['options']);
        gulp.watch('src/js/bg.js', ['bg']);
        gulp.watch('src/css/style.css', ['mincss']);
    });

    var extDest = './extension';

    gulp.task('production', function () {
        //Манифест
        gulp.src('./manifest.json').pipe(gulp.dest(extDest));
        //Стили
        gulp.src(['./src/css/*/**', './src/css/style.min.css']).pipe(gulp.dest(extDest + '/src/css'));
        //Картинки
        gulp.src(['./src/images/**']).pipe(gulp.dest(extDest + '/src/images'));
        //Скрипты
        gulp.src(['./src/js/min/*.js']).pipe(gulp.dest(extDest + '/src/js/min'));
        //html страницы
        gulp.src(['./src/htdocs/*.html']).pipe(gulp.dest(extDest + '/htdocs'));
        //Делаем архив
        gulp.src('./extension/**').pipe(zip('extension.zip')).pipe(gulp.dest('./'));

        gulp.src(['./src/htdocs/*.html']).pipe(gulp.dest(extDest + '/src/htdocs'));
        //Архив
        gulp.src(extDest + '/**').pipe(zip('extension.zip')).pipe(gulp.dest('./'));
    });
}(require));