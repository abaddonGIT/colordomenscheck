/**
 * Created by abaddon on 04.02.2015.
 */
(function (ch, d) {
    var React = require('react');
    var PopupTable = require('./components/popupTable.jsx');

    React.render(React.createElement(PopupTable), d.querySelector('#tablePlace'));
}(chrome, document));