(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
/**
 * Created by abaddon on 03.02.2015.
 */
var Shara = require('./venders/sharedMethods.js'),
    notification = require('./venders/notificationApi.js'),
    log = require('./venders/log'),
    st = require('./venders/storage'),
    config = require('./config');

(function (ch, w) {
    var Background = function () {
        this.config = {
            domainUrl: 'http://who.is/whois/'
        };
        this.updateOptions();
        this.domains = [];
    };

    Background.prototype = {
        updateOptions: function () {
            st.get('config').then(function (config) {
                this.options = config;
            }.bind(this));
        },
        clear: function () {
            clearTimeout(this.timer);
            st.remove('domains');
        },
        fromString: function () {
            var loc = this.domains, ln = loc.length, that = this, ident = Shara.getIdentificator(), identStamp = Shara.getIdentificator(true);
            that.updateOptions();
            var _send = function (i) {
                if (i < ln) {
                    if (loc[i].timestamp !== ident) {
                        console.log('req');
                        setTimeout(function () {
                            Shara.ajax(loc[i].name).then(function (response) {
                                st.set('lock', false);
                                return new Promise(function (resolve, reject) {
                                    var item = that.stringFunction.call(that, Shara, response, loc[i].name);
                                    if (item) {
                                        if (item) {
                                            var difference = item.difference;
                                            if (difference <= that.options.dangerosLevel) {
                                                item.status = 1;
                                            } else if (difference <= that.options.warnLevel) {
                                                item.status = 2;
                                            } else if (difference === false) {
                                                item.status = 3;
                                            } else {
                                                item.status = 0;
                                            }
                                            item.timestamp = ident;
                                            loc[i] = item;
                                            i++;
                                            resolve(_send(i));
                                        }
                                    }
                                });
                            });
                        }, Shara.random(3000, 5000));
                    } else {
                        i++;
                        _send(i);
                    }
                } else {
                    st.set({'domains': loc, 'loading': false});
                    console.log('finish');
                    that.domains = loc;
                    if (that.options.notification) {
                        notification.show(loc);
                    }
                    if (loc.length) {
                        that.timer = setTimeout(function () {
                            that.fromString();
                        }, 60000);
                    }
                }
            };
            _send(0);
        }
    };
    w.onload = function () {
        var back = new Background(),
            checkDomainList = function () {
                st.get('domains').then(function (domains) {
                    if (domains.length) {
                        back.domains = domains;
                        back.fromString();
                    }
                });

                Shara.getFunction().then(function (func) {
                    back.stringFunction = func;
                });
            };

        chrome.extension.onRequest.addListener(
            function (request, sender, sendResponse) {
                switch (request.operation) {
                    case 'file':
                        back.clear();
                        back.domains = request.domenlist;
                        back.fromString();
                        sendResponse('Файл с доменами загружен!');
                        break;
                    case 'refresh':
                        st.get('domains').then(function (domains) {
                            var ln = domains.length;
                            if (domains.length) {
                                clearTimeout(back.timer);
                                for (var i = 0; i < ln; i++) {
                                    domains[i].timestamp = 0;
                                }
                                st.set('domains', domains);
                                back.domains = domains;
                                back.fromString();
                            }
                        });
                        sendResponse('Кэш сброшен!');
                        break;
                    case 'addDomen':
                        var domen = request.domen;
                        if (!back.domains.length) {
                            back.domains.push({
                                name: domen
                            });
                        } else {
                            back.domains.push({
                                name: domen
                            });
                        }
                        clearTimeout(back.timer);
                        back.fromString();
                        sendResponse('Домен добавлен!');
                        break;
                    case 'removeDomen':
                        var localDomains = back.domains, ln = localDomains.length, removed = request.removed, newDomains = [];
                        if (ln) {
                            while (ln--) {
                                var name = localDomains[ln].name;
                                if (removed[name]) {
                                    delete localDomains[ln];
                                }
                            }
                            localDomains.forEach(function (item) {
                                newDomains.push(item);
                            });
                            back.domains = newDomains;
                            clearTimeout(back.timer);
                            back.fromString();
                        }
                        sendResponse(back.domains);
                        break;
                    case 'relodeParser':
                        Shara.getFunction().then(function (func) {
                            clearTimeout(back.timer);
                            back.stringFunction = func;
                            back.fromString();
                            sendResponse('Парсер перезагружен!');
                        });
                        break;
                    case 'updateDomen':
                        var updateName = request.name, ln = back.domains.length;
                        for (var i = 0; i < ln; i++) {
                            var loc = back.domains[i];
                            if (loc.name === updateName) {
                                back.domains[i].timestamp = 0;
                            }
                        }
                        st.set('domains', back.domains);
                        back.fromString();
                        sendResponse(true);
                        break;
                }
            }
        );

        //Вытаскиваем ф-ю парсинга
        checkDomainList();
    };
}(chrome, window));
},{"./config":2,"./venders/log":3,"./venders/notificationApi.js":4,"./venders/sharedMethods.js":5,"./venders/storage":6}],2:[function(require,module,exports){
/**
 * Created by abaddon on 13.02.2015.
 */
module.exports = {
    optionPage: {
        domains: [],
        lock: false,
        progress: 0,
        config: {
            debag: false,
            warnLevel: 30,
            dangerosLevel: 7,
            warnColor: '#ffff00',
            dengerosColor: '#ff0000',
            goodColor: '#32CD32',
            notification: true
        },
        message: ""
    },
    codPlace: {
        cod: '',
        ace: '',
        list: false
    },
    codePattern: {
        blank: {
            code: "return false;"
        },
        default: {
            url: 'http://whois7.ru/',
            domenParam: 'q',
            method: 'GET',
            params: [],
            code: "var identStamp = Shara.getIdentificator(true), difference;\r\nvar oneParse = function (name) {\r\n    var firstTemp = new RegExp(name + '\\\\:(.*)', 'g');\r\n    firstRes = firstTemp.exec(response);\r\n    if (firstRes) {\r\n        var secondTemp = new RegExp('(\\\\d{4})\\\\.(\\\\d{1,2})\\\\.(\\\\d{1,2})', 'g');\r\n        var date = secondTemp.exec(firstRes[0]);\r\n        if (date) {\r\n            return {\r\n                year: date[1],\r\n                month: date[2],\r\n                day: date[3]\r\n            };\r\n        } else {\r\n            return false;\r\n        }\r\n    } else {\r\n        return false;\r\n    }   \r\n};\r\nvar twoParse = function (name) {\r\n    var firstTemp = new RegExp(name + '\\\\:(.*)', 'g');\r\n    firstRes = firstTemp.exec(response);\r\n    if (firstRes) {\r\n        var date = new Date(firstRes[1]);\r\n        if (date) { \r\n            return {\r\n                year: date.getFullYear(),\r\n                month: date.getMonth() + 1,\r\n                day: date.getUTCDate()\r\n            };\r\n        } else {\r\n            return false;\r\n        }\r\n    } else {\r\n        return false;\r\n    }\r\n};\r\nvar getDifferent = function (time) {\r\n    var stamp;\r\n    if (time instanceof Object) {\r\n        stamp = new Date(time.month + '/' + time.day + '/' + time.year).getTime();\r\n    } else {\r\n        stamp = new Date(time).getTime();\r\n    }\r\n    \r\n    var diff = Math.ceil((stamp - identStamp) / (1000 * 60 * 60 * 24));\r\n    return diff;\r\n};\r\nvar created = oneParse('created') || twoParse('Created On'),\r\n    paidTill = oneParse('paid-till') || twoParse('Expiration Date'),\r\n    freeDate = oneParse('free-date') || {};\r\nif (paidTill) {\r\n    difference = getDifferent(paidTill);    \r\n} else {\r\n    difference = false;\r\n}\r\nvar result = {\r\n    name: domenName,\r\n    createDate: created.day ? (created.day + '.' + created.month + '.' + created.year) : \"\",\r\n    paidTill: paidTill.day ? (paidTill.day + '.' + paidTill.month + '.' + paidTill.year) : \"\",\r\n    freeDate: freeDate.day ? (freeDate.day + '.' + freeDate.month + '.' + freeDate.year) : \"\",\r\n    difference: difference ? difference : false\r\n}\r\n\r\nreturn result;"
        }
    }
};
},{}],3:[function(require,module,exports){
/**
 * Created by abaddon on 17.02.2015.
 * Logging messages api
 * global console
 */
var st = require('./storage'), log;
(function (s) {
    "use strict";
    log = function (text, type) {
        s.get('config').then(function (config) {
            if (config.debag) {
                switch (type) {
                    case 'error':
                        console.error(text);
                        break;
                    case 'warn':
                        console.warn(text);
                        break;
                    default:
                        console.log(text);
                }
            }
        });
    };
}(st));
module.exports = log;


},{"./storage":6}],4:[function(require,module,exports){
/**
 * Created by abaddon on 08.02.2015.
 */
var notificationApi = {
    notification: {
        type: "basic",
        title: 'Срок оплаты!!!',
        message: '',
        iconUrl: "../images/icon_48.png"
    },
    show: function (list) {
        var ln = list.length, domainList = [], config = this.notification;
        while (ln--) {
            var loc = list[ln];
            if (loc.status === 1 || loc.status === 2) {
                domainList.push(loc.name);
            }
        }
        if (domainList.length) {
            config.message = 'Приближается срок оплаты для доменов: ' + domainList.join(', ');
            chrome.notifications.getAll(function (notifications) {
                if (notifications['color']) {
                    chrome.notifications.clear('color', function (wasCleared) {
                        chrome.notifications.create('color', config, function () {
                        });
                    });
                } else {
                    chrome.notifications.create('color', config, function () {
                    });
                }
            });
        }
    }
};
module.exports = notificationApi;
},{}],5:[function(require,module,exports){
/**
 * Created by abaddon on 31.01.2015.
 */
var config = require('../config'),
    st = require('./storage');

module.exports = {
    relode: function () {
        window.location.reload();
    },
    clearArray: function (arr) {
        var outArr = [];
        arr.forEach(function (v) {
            v = v.replace(/\s/g, "");
            if (v) {
                var item = v.split(';');
                if (item[0] && v) {
                    outArr.push({
                        name: item[0],
                        createDate: "",
                        paidTill: "",
                        freeDate: ""
                    });
                }
            }
        });
        return outArr;
    },
    getIdentificator: function (stamp) {
        if (stamp) {
            return Date.now();
        } else {
            var currDate = new Date();
            return currDate.getDate() + '_' + (currDate.getMonth() + 1) + '_' + currDate.getFullYear();
        }
    },
    validate: {
        isDomen: function (text) {
            return /^([\a-zа-яё0-9\.-]+)\.([a-zа-яё\.]{2,6})([\/\w \.-]*)*\/?$/i.test(text);
        },
        isEmpty: function (text) {
            return /^[a-zа-яё0-9\?\&\=]/i.test(text);
        }
    },
    ajax: function (domainName) {
        var that = this, xhr;
        return new Promise(function (resolve, reject) {
            that.getActiveParser().then(function (parser) {
                var url = parser.url, params;
                try {
                    params = JSON.parse(parser.params);
                } catch (e) {
                    params = {};
                }

                switch (parser.method) {
                    case 'POST':
                        var form = new FormData();
                        form.append(parser.domenParam, domainName);
                        for (var i in params) {
                            form.append(i, params[i]);
                        }
                        break;
                    case 'GET':
                        url += parser.domenParam ? '?' + parser.domenParam + '=' + domainName : domainName;
                        var dop = '';
                        for (var i in params) {
                            dop += i + '=' + params[i] + '&';
                        }
                        dop = dop.substr(0, dop.length - 1);
                        if (!parser.domenParam) {
                            url += '?';
                        } else {
                            if (dop) {
                                url += '&';
                            }
                        }
                        url += dop;
                        break;
                }

                st.set({'loading': true, 'lock': true});
                xhr = new XMLHttpRequest();
                xhr.open(parser.method, url, false);
                xhr.onreadystatechange = function (e) {
                    if (this.readyState == 4 && this.status == 200) {
                        resolve(this.response);
                    } else {
                        resolve(false);
                    }
                };
                xhr.onerror = function (e) {
                    reject(false);
                };
                parser.method === 'POST' ? xhr.send(form) : xhr.send();
            });
        });
    },
    pageParsers: {
        whoisParseDates: function (name, string) {
            var firstTemp = new RegExp(name + '\\:(.*)<br>', 'g'),
                firstRes = firstTemp.exec(string);
            if (firstRes) {
                var secondTemp = new RegExp('(\\d{4})\\.(\\d{1,2})\\.(\\d{1,2})', 'g');
                var date = secondTemp.exec(firstRes[0]);
                return {
                    year: date[1],
                    month: date[2],
                    day: date[3]
                };
            } else {
                return false;
            }
        }
    },
    inArray: function (array, name) {
        var ln = array.length;
        while (ln--) {
            loc = array[ln];
            if (loc.name === name) {
                return true;
            }
        }
    },
    random: function (min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    },
    getFunction: function () {
        return new Promise(function (resolve) {
            this.getActiveParser().then(function (parser) {
                var cod;
                try {
                    cod = JSON.parse(parser.cod);
                } catch (e) {
                    cod = parser.cod;
                }
                var func = new Function('Shara', 'response', 'domenName', cod);
                resolve(func);
            });
        }.bind(this));
    },
    getActiveParser: function () {
        return new Promise(function (resolve, reject) {
            var that = this;
            st.get('parsers').then(function (parsers) {
                if (!parsers.length) {/*Если хранилище пустое то пишем по умолчанию*/
                    var item = {
                        name: 'default',
                        url: config.codePattern.default.url,
                        cod: config.codePattern.default.code,
                        method: config.codePattern.default.method,
                        domenParam: config.codePattern.default.domenParam,
                        params: config.codePattern.default.params,
                        active: true
                    };
                    parsers = [];
                    parsers.push(item);
                    st.set('parsers', parsers);
                    resolve(item);
                } else {
                    var ln = parsers.length;
                    while (ln--) {
                        var loc = parsers[ln];
                        if (loc.active) {
                            resolve(loc);
                            return;
                        }
                    }
                }
            });
        }.bind(this));
    },
    setActiveParser: function (name) {
        var that = this;
        st.get('parsers').then(function (parsers) {
            var ln = parsers.length;
            for (var i = 0; i < ln; i++) {
                parsers[i].name === name ? parsers[i].active = true : parsers[i].active = false;
                if (name === 'def') {
                    parsers[i] = {
                        name: 'default',
                        url: config.codePattern.default.url,
                        cod: config.codePattern.default.code,
                        method: config.codePattern.default.method,
                        domenParam: config.codePattern.default.domenParam,
                        params: config.codePattern.default.params,
                        active: true
                    };
                }
            }
            st.set('parsers', parsers);
        });
    },
    saveChangeActiveParser: function (cod) {
        var cod = JSON.stringify(cod), that = this;
        st.get('parsers').then(function (parsers) {
            var ln = parsers.length;
            for (var i = 0; i < ln; i++) {
                if (parsers[i].active) {
                    parsers[i].cod = cod;
                    break;
                }
            }
            st.set('parsers', parsers);
        });
    },
    isParser: function (name) {
        var that = this;
        return new Promise(function (resolve, reject) {
            st.get('parsers').then(function (parsers) {
                var ln = parsers.length;
                while (ln--) {
                    if (parsers[ln].name === name) {
                        resolve(true);
                        return;
                    }
                }
                resolve(false);
            });
        });
    },
    addNewParser: function (newParser, cb) {
        var that = this;
        newParser.cod = config.codePattern.blank.code;

        st.get('parsers').then(function (parsers) {
            parsers.push(newParser);
            st.set('parsers', parsers);
            cb();
        });
    },
    changeParser: function (options) {
        var that = this;
        return new Promise(function (resolve, reject) {
            st.get('parsers').then(function (parsers) {
                var ln = parsers.length;
                for (var i = 0; i < ln; i++) {
                    if (parsers[i].name === options.name) {
                        parsers[i].url = options.url;
                        parsers[i].domenParam = options.domenParam;
                        parsers[i].method = options.method;
                        parsers[i].params = options.params;
                    }
                }
                st.set('parsers', parsers);
                resolve();
            });
        });
    }
};
},{"../config":2,"./storage":6}],6:[function(require,module,exports){
/**
 * Created by abaddon on 17.02.2015.
 *  Storage api
 * global chrome
 */
"use strict";
module.exports = {
    storage: chrome.storage.local,
    error: chrome.runtime.lastError,
    set: function (name, value) {
        if (typeof name === 'string') {
            var save = {};
            save[name] = value;
        } else {
            save = name;
        }
        this.storage.set(save, function () {
            if (this.error) {
                console.error('when trying to write an error occurred' + error.message, 'error');
            }
        }.bind(this));
    },
    get: function (name) {
        return new Promise(function (res, rej) {
            this.storage.get(name, function (item) {
                if (item[name]) {
                    res(item[name]);
                } else {
                    res(false);
                }
            });
        }.bind(this));
    },
    remove: function (name) {
        this.storage.remove(name);
    },
    size: function (name) {
        return new Promise(function (res) {
            this.storage.getBytesInUse(name, function (bytesInUse) {
                res(bytesInUse);
            });
        }.bind(this));
    }
};

},{}]},{},[1]);
