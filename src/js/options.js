/**
 * Created by abaddon on 03.02.2015.
 */
(function (d) {
    var React = require('react');
    var FileField = require('./components/FileField.jsx');
    var CodPlace = require('./components/CodPlace.jsx');

    React.render(React.createElement(FileField), d.querySelector("#work-space"));
    React.render(React.createElement(CodPlace), d.querySelector("#cod-space"));
}(document));