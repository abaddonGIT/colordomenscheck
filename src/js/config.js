/**
 * Created by abaddon on 13.02.2015.
 */
module.exports = {
    optionPage: {
        domains: [],
        lock: false,
        progress: 0,
        config: {
            debag: false,
            warnLevel: 30,
            dangerosLevel: 7,
            warnColor: '#ffff00',
            dengerosColor: '#ff0000',
            goodColor: '#32CD32',
            notification: true
        },
        message: ""
    },
    codPlace: {
        cod: '',
        ace: '',
        list: false
    },
    codePattern: {
        blank: {
            code: "return false;"
        },
        default: {
            url: 'http://whois7.ru/',
            domenParam: 'q',
            method: 'GET',
            params: [],
            code: "var identStamp = Shara.getIdentificator(true), difference;\r\nvar oneParse = function (name) {\r\n    var firstTemp = new RegExp(name + '\\\\:(.*)', 'g');\r\n    firstRes = firstTemp.exec(response);\r\n    if (firstRes) {\r\n        var secondTemp = new RegExp('(\\\\d{4})\\\\.(\\\\d{1,2})\\\\.(\\\\d{1,2})', 'g');\r\n        var date = secondTemp.exec(firstRes[0]);\r\n        if (date) {\r\n            return {\r\n                year: date[1],\r\n                month: date[2],\r\n                day: date[3]\r\n            };\r\n        } else {\r\n            return false;\r\n        }\r\n    } else {\r\n        return false;\r\n    }   \r\n};\r\nvar twoParse = function (name) {\r\n    var firstTemp = new RegExp(name + '\\\\:(.*)', 'g');\r\n    firstRes = firstTemp.exec(response);\r\n    if (firstRes) {\r\n        var date = new Date(firstRes[1]);\r\n        if (date) { \r\n            return {\r\n                year: date.getFullYear(),\r\n                month: date.getMonth() + 1,\r\n                day: date.getUTCDate()\r\n            };\r\n        } else {\r\n            return false;\r\n        }\r\n    } else {\r\n        return false;\r\n    }\r\n};\r\nvar getDifferent = function (time) {\r\n    var stamp;\r\n    if (time instanceof Object) {\r\n        stamp = new Date(time.month + '/' + time.day + '/' + time.year).getTime();\r\n    } else {\r\n        stamp = new Date(time).getTime();\r\n    }\r\n    \r\n    var diff = Math.ceil((stamp - identStamp) / (1000 * 60 * 60 * 24));\r\n    return diff;\r\n};\r\nvar created = oneParse('created') || twoParse('Created On'),\r\n    paidTill = oneParse('paid-till') || twoParse('Expiration Date'),\r\n    freeDate = oneParse('free-date') || {};\r\nif (paidTill) {\r\n    difference = getDifferent(paidTill);    \r\n} else {\r\n    difference = false;\r\n}\r\nvar result = {\r\n    name: domenName,\r\n    createDate: created.day ? (created.day + '.' + created.month + '.' + created.year) : \"\",\r\n    paidTill: paidTill.day ? (paidTill.day + '.' + paidTill.month + '.' + paidTill.year) : \"\",\r\n    freeDate: freeDate.day ? (freeDate.day + '.' + freeDate.month + '.' + freeDate.year) : \"\",\r\n    difference: difference ? difference : false\r\n}\r\n\r\nreturn result;"
        }
    }
};