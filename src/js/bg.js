/**
 * Created by abaddon on 03.02.2015.
 */
var Shara = require('./venders/sharedMethods.js'),
    notification = require('./venders/notificationApi.js'),
    log = require('./venders/log'),
    st = require('./venders/storage'),
    config = require('./config');

(function (ch, w) {
    var Background = function () {
        this.config = {
            domainUrl: 'http://who.is/whois/'
        };
        this.updateOptions();
        this.domains = [];
    };

    Background.prototype = {
        updateOptions: function () {
            st.get('config').then(function (config) {
                this.options = config;
            }.bind(this));
        },
        clear: function () {
            clearTimeout(this.timer);
            st.remove('domains');
        },
        fromString: function () {
            var loc = this.domains, ln = loc.length, that = this, ident = Shara.getIdentificator(), identStamp = Shara.getIdentificator(true);
            that.updateOptions();
            var _send = function (i) {
                if (i < ln) {
                    if (loc[i].timestamp !== ident) {
                        console.log('req');
                        setTimeout(function () {
                            Shara.ajax(loc[i].name).then(function (response) {
                                st.set('lock', false);
                                return new Promise(function (resolve, reject) {
                                    var item = that.stringFunction.call(that, Shara, response, loc[i].name);
                                    if (item) {
                                        if (item) {
                                            var difference = item.difference;
                                            if (difference <= that.options.dangerosLevel) {
                                                item.status = 1;
                                            } else if (difference <= that.options.warnLevel) {
                                                item.status = 2;
                                            } else if (difference === false) {
                                                item.status = 3;
                                            } else {
                                                item.status = 0;
                                            }
                                            item.timestamp = ident;
                                            loc[i] = item;
                                            i++;
                                            resolve(_send(i));
                                        }
                                    }
                                });
                            });
                        }, Shara.random(3000, 5000));
                    } else {
                        i++;
                        _send(i);
                    }
                } else {
                    st.set({'domains': loc, 'loading': false});
                    console.log('finish');
                    that.domains = loc;
                    if (that.options.notification) {
                        notification.show(loc);
                    }
                    if (loc.length) {
                        that.timer = setTimeout(function () {
                            that.fromString();
                        }, 60000);
                    }
                }
            };
            _send(0);
        }
    };
    w.onload = function () {
        var back = new Background(),
            checkDomainList = function () {
                st.get('domains').then(function (domains) {
                    if (domains.length) {
                        back.domains = domains;
                        back.fromString();
                    }
                });

                Shara.getFunction().then(function (func) {
                    back.stringFunction = func;
                });
            };

        chrome.extension.onRequest.addListener(
            function (request, sender, sendResponse) {
                switch (request.operation) {
                    case 'file':
                        back.clear();
                        back.domains = request.domenlist;
                        back.fromString();
                        sendResponse('Файл с доменами загружен!');
                        break;
                    case 'refresh':
                        st.get('domains').then(function (domains) {
                            var ln = domains.length;
                            if (domains.length) {
                                clearTimeout(back.timer);
                                for (var i = 0; i < ln; i++) {
                                    domains[i].timestamp = 0;
                                }
                                st.set('domains', domains);
                                back.domains = domains;
                                back.fromString();
                            }
                        });
                        sendResponse('Кэш сброшен!');
                        break;
                    case 'addDomen':
                        var domen = request.domen;
                        if (!back.domains.length) {
                            back.domains.push({
                                name: domen
                            });
                        } else {
                            back.domains.push({
                                name: domen
                            });
                        }
                        clearTimeout(back.timer);
                        back.fromString();
                        sendResponse('Домен добавлен!');
                        break;
                    case 'removeDomen':
                        var localDomains = back.domains, ln = localDomains.length, removed = request.removed, newDomains = [];
                        if (ln) {
                            while (ln--) {
                                var name = localDomains[ln].name;
                                if (removed[name]) {
                                    delete localDomains[ln];
                                }
                            }
                            localDomains.forEach(function (item) {
                                newDomains.push(item);
                            });
                            back.domains = newDomains;
                            clearTimeout(back.timer);
                            back.fromString();
                        }
                        sendResponse(back.domains);
                        break;
                    case 'relodeParser':
                        Shara.getFunction().then(function (func) {
                            clearTimeout(back.timer);
                            back.stringFunction = func;
                            back.fromString();
                            sendResponse('Парсер перезагружен!');
                        });
                        break;
                    case 'updateDomen':
                        var updateName = request.name, ln = back.domains.length;
                        for (var i = 0; i < ln; i++) {
                            var loc = back.domains[i];
                            if (loc.name === updateName) {
                                back.domains[i].timestamp = 0;
                            }
                        }
                        st.set('domains', back.domains);
                        back.fromString();
                        sendResponse(true);
                        break;
                }
            }
        );

        //Вытаскиваем ф-ю парсинга
        checkDomainList();
    };
}(chrome, window));