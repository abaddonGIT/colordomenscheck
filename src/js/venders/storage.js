/**
 * Created by abaddon on 17.02.2015.
 *  Storage api
 * global chrome
 */
"use strict";
module.exports = {
    storage: chrome.storage.local,
    error: chrome.runtime.lastError,
    set: function (name, value) {
        if (typeof name === 'string') {
            var save = {};
            save[name] = value;
        } else {
            save = name;
        }
        this.storage.set(save, function () {
            if (this.error) {
                console.error('when trying to write an error occurred' + error.message, 'error');
            }
        }.bind(this));
    },
    get: function (name) {
        return new Promise(function (res, rej) {
            this.storage.get(name, function (item) {
                if (item[name]) {
                    res(item[name]);
                } else {
                    res(false);
                }
            });
        }.bind(this));
    },
    remove: function (name) {
        this.storage.remove(name);
    },
    size: function (name) {
        return new Promise(function (res) {
            this.storage.getBytesInUse(name, function (bytesInUse) {
                res(bytesInUse);
            });
        }.bind(this));
    }
};
