/**
 * Created by abaddon on 08.02.2015.
 */
var notificationApi = {
    notification: {
        type: "basic",
        title: 'Срок оплаты!!!',
        message: '',
        iconUrl: "../images/icon_48.png"
    },
    show: function (list) {
        var ln = list.length, domainList = [], config = this.notification;
        while (ln--) {
            var loc = list[ln];
            if (loc.status === 1 || loc.status === 2) {
                domainList.push(loc.name);
            }
        }
        if (domainList.length) {
            config.message = 'Приближается срок оплаты для доменов: ' + domainList.join(', ');
            chrome.notifications.getAll(function (notifications) {
                if (notifications['color']) {
                    chrome.notifications.clear('color', function (wasCleared) {
                        chrome.notifications.create('color', config, function () {
                        });
                    });
                } else {
                    chrome.notifications.create('color', config, function () {
                    });
                }
            });
        }
    }
};
module.exports = notificationApi;