/**
 * Created by abaddon on 31.01.2015.
 */
var config = require('../config'),
    st = require('./storage');

module.exports = {
    relode: function () {
        window.location.reload();
    },
    clearArray: function (arr) {
        var outArr = [];
        arr.forEach(function (v) {
            v = v.replace(/\s/g, "");
            if (v) {
                var item = v.split(';');
                if (item[0] && v) {
                    outArr.push({
                        name: item[0],
                        createDate: "",
                        paidTill: "",
                        freeDate: ""
                    });
                }
            }
        });
        return outArr;
    },
    getIdentificator: function (stamp) {
        if (stamp) {
            return Date.now();
        } else {
            var currDate = new Date();
            return currDate.getDate() + '_' + (currDate.getMonth() + 1) + '_' + currDate.getFullYear();
        }
    },
    validate: {
        isDomen: function (text) {
            return /^([\a-zа-яё0-9\.-]+)\.([a-zа-яё\.]{2,6})([\/\w \.-]*)*\/?$/i.test(text);
        },
        isEmpty: function (text) {
            return /^[a-zа-яё0-9\?\&\=]/i.test(text);
        }
    },
    ajax: function (domainName) {
        var that = this, xhr;
        return new Promise(function (resolve, reject) {
            that.getActiveParser().then(function (parser) {
                var url = parser.url, params;
                try {
                    params = JSON.parse(parser.params);
                } catch (e) {
                    params = {};
                }

                switch (parser.method) {
                    case 'POST':
                        var form = new FormData();
                        form.append(parser.domenParam, domainName);
                        for (var i in params) {
                            form.append(i, params[i]);
                        }
                        break;
                    case 'GET':
                        url += parser.domenParam ? '?' + parser.domenParam + '=' + domainName : domainName;
                        var dop = '';
                        for (var i in params) {
                            dop += i + '=' + params[i] + '&';
                        }
                        dop = dop.substr(0, dop.length - 1);
                        if (!parser.domenParam) {
                            url += '?';
                        } else {
                            if (dop) {
                                url += '&';
                            }
                        }
                        url += dop;
                        break;
                }

                st.set('loading', true);
                xhr = new XMLHttpRequest();
                xhr.open(parser.method, url, false);
                xhr.onreadystatechange = function (e) {
                    if (this.readyState == 4 && this.status == 200) {
                        resolve(this.response);
                    } else {
                        resolve(false);
                    }
                };
                xhr.onerror = function (e) {
                    reject(false);
                };
                parser.method === 'POST' ? xhr.send(form) : xhr.send();
            });
        });
    },
    pageParsers: {
        whoisParseDates: function (name, string) {
            var firstTemp = new RegExp(name + '\\:(.*)<br>', 'g'),
                firstRes = firstTemp.exec(string);
            if (firstRes) {
                var secondTemp = new RegExp('(\\d{4})\\.(\\d{1,2})\\.(\\d{1,2})', 'g');
                var date = secondTemp.exec(firstRes[0]);
                return {
                    year: date[1],
                    month: date[2],
                    day: date[3]
                };
            } else {
                return false;
            }
        }
    },
    inArray: function (array, name) {
        var ln = array.length;
        while (ln--) {
            loc = array[ln];
            if (loc.name === name) {
                return true;
            }
        }
    },
    random: function (min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    },
    getFunction: function () {
        return new Promise(function (resolve) {
            this.getActiveParser().then(function (parser) {
                var cod;
                try {
                    cod = JSON.parse(parser.cod);
                } catch (e) {
                    cod = parser.cod;
                }
                var func = new Function('Shara', 'response', 'domenName', cod);
                resolve(func);
            });
        }.bind(this));
    },
    getActiveParser: function () {
        return new Promise(function (resolve, reject) {
            var that = this;
            st.get('parsers').then(function (parsers) {
                if (!parsers.length) {/*Если хранилище пустое то пишем по умолчанию*/
                    var item = {
                        name: 'default',
                        url: config.codePattern.default.url,
                        cod: config.codePattern.default.code,
                        method: config.codePattern.default.method,
                        domenParam: config.codePattern.default.domenParam,
                        params: config.codePattern.default.params,
                        active: true
                    };
                    parsers = [];
                    parsers.push(item);
                    st.set('parsers', parsers);
                    resolve(item);
                } else {
                    var ln = parsers.length;
                    while (ln--) {
                        var loc = parsers[ln];
                        if (loc.active) {
                            resolve(loc);
                            return;
                        }
                    }
                }
            });
        }.bind(this));
    },
    setActiveParser: function (name) {
        var that = this;
        st.get('parsers').then(function (parsers) {
            var ln = parsers.length;
            for (var i = 0; i < ln; i++) {
                parsers[i].name === name ? parsers[i].active = true : parsers[i].active = false;
                if (name === 'def') {
                    parsers[i] = {
                        name: 'default',
                        url: config.codePattern.default.url,
                        cod: config.codePattern.default.code,
                        method: config.codePattern.default.method,
                        domenParam: config.codePattern.default.domenParam,
                        params: config.codePattern.default.params,
                        active: true
                    };
                }
            }
            st.set('parsers', parsers);
        });
    },
    saveChangeActiveParser: function (cod) {
        var cod = JSON.stringify(cod), that = this;
        st.get('parsers').then(function (parsers) {
            var ln = parsers.length;
            for (var i = 0; i < ln; i++) {
                if (parsers[i].active) {
                    parsers[i].cod = cod;
                    break;
                }
            }
            st.set('parsers', parsers);
        });
    },
    isParser: function (name) {
        var that = this;
        return new Promise(function (resolve, reject) {
            st.get('parsers').then(function (parsers) {
                var ln = parsers.length;
                while (ln--) {
                    if (parsers[ln].name === name) {
                        resolve(true);
                        return;
                    }
                }
                resolve(false);
            });
        });
    },
    addNewParser: function (newParser, cb) {
        var that = this;
        newParser.cod = config.codePattern.blank.code;

        st.get('parsers').then(function (parsers) {
            parsers.push(newParser);
            st.set('parsers', parsers);
            cb();
        });
    },
    changeParser: function (options) {
        var that = this;
        return new Promise(function (resolve, reject) {
            st.get('parsers').then(function (parsers) {
                var ln = parsers.length;
                for (var i = 0; i < ln; i++) {
                    if (parsers[i].name === options.name) {
                        parsers[i].url = options.url;
                        parsers[i].domenParam = options.domenParam;
                        parsers[i].method = options.method;
                        parsers[i].params = options.params;
                    }
                }
                st.set('parsers', parsers);
                resolve();
            });
        });
    }
};