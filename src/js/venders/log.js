/**
 * Created by abaddon on 17.02.2015.
 * Logging messages api
 * global console
 */
var st = require('./storage'), log;
(function (s) {
    "use strict";
    log = function (text, type) {
        s.get('config').then(function (config) {
            if (config.debag) {
                switch (type) {
                    case 'error':
                        console.error(text);
                        break;
                    case 'warn':
                        console.warn(text);
                        break;
                    default:
                        console.log(text);
                }
            }
        });
    };
}(st));
module.exports = log;

