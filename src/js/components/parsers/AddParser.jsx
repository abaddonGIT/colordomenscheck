var React = require('react'),
    config = require('../../config'),
    Shara = require('../../venders/sharedMethods.js'),
    Mixins = require('../mixins.jsx');

var AddParser = React.createClass({
    mixins: [Mixins],
    addNewParserHandler: function (e) {
        var name = this.refs.name.getDOMNode().value, newParser = {};
        Shara.isParser(name).then(function (is) {
            if (is) {
                alert('Парсер с таким именем уже существует!');
            } else {
                for (var ref in this.refs) {
                    var value = this.refs[ref].getDOMNode().value.trim(), need = this.refs[ref].getDOMNode().getAttribute('data-need');
                    if (!Mixins.validate(value, 'isEmpty') && need) {
                        alert('Недопустимое значение поля ' + ref + "!");
                        return;
                    } else {
                        newParser[ref] = value.replace(/\s/g, "");
                    }
                }

                Shara.addNewParser(newParser, function () {
                    Shara.setActiveParser(newParser.name);
                    Shara.relode();
                }.bind(this));
            }
        }.bind(this));
        e.preventDefault();
    },
    render: function () {
        return (
            <form>
                <table className="parserTable">
                    <tr>
                        <th>Метка</th>
                        <th>URL запроса</th>
                        <th>Метод</th>
                        <th>Параметр для имени домена</th>
                        <th>Параметры запроса</th>
                        <th></th>
                    </tr>
                    <tr>
                        <td>
                            <input type="text" name="name" ref="name" data-need="true" />
                        </td>
                        <td>
                            <input type="text" name="url" ref="url" data-need="true" />
                        </td>
                        <td>
                            <select name="method" ref="method">
                                <option value="GET" defaultValue="selected">GET</option>
                                <option value="POST">POST</option>
                            </select>
                        </td>
                        <td>
                            <input type="text" name="domenParam" ref="domenParam" />
                        </td>
                        <td>
                            <textarea ref="params"></textarea>
                        </td>
                        <td>
                            <i onClick={this.addNewParserHandler} className="fa fa-plus-circle fa-3x"></i>
                        </td>
                    </tr>
                </table>
            </form>
            );
    }
});

module.exports = AddParser;