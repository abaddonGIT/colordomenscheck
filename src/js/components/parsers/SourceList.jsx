var React = require('react'),
    st = require('../../venders/storage'),
    log = require('../../venders/log'),
    config = require('../../config'),
    Shara = require('../../venders/sharedMethods.js'),
    Mixins = require('../mixins.jsx');

var SourceList = React.createClass({
    mixins: [Mixins],
    getInitialState: function () {
        return {
            parsers: []
        }
    },
    componentWillMount: function () {
        var timer, _get = function () {
            st.get('parsers').then(function (parsers) {
                if (parsers.length) {
                    this.setState({parsers: parsers});
                    clearTimeout(timer);
                } else {
                    timer = setTimeout(function () {
                        _get.call(this);
                    }.bind(this), 1000);
                }
            }.bind(this));
        };
        _get.call(this);
    },
    editHandler: function (e) {
        var tr = e.target.parentNode.parentNode;
        tr.classList.add('editable');
        e.preventDefault();
    },
    cancelHandler: function (e) {
        var tr = e.target.parentNode.parentNode;
        tr.classList.remove('editable');
        e.preventDefault();
    },
    saveHandler: function (e) {
        var newParser = {}, tr = e.target.parentNode.parentNode;

        var inputs = tr.querySelectorAll('input.data'), select = tr.querySelector('select'), textarea = tr.querySelector('textarea'), ln = inputs.length;
        for (var i = 0; i < ln; i++) {
            var loc = inputs[i], need = loc.getAttribute('data-need'), name = loc.name, value = loc.value.trim();
            if (!Mixins.validate(value, 'isEmpty') && need) {
                alert('Недопустимое значение поля ' + name + "!");
                return;
            } else {
                newParser[name] = value.replace(/\s/g, "");
            }
        }
        newParser[select.name] = select.value;
        newParser[textarea.name] = textarea.value;
        Shara.changeParser(newParser).then(function () {
            Shara.relode();
        });
        e.preventDefault();
    },
    changeParserHandler: function (name, e) {
        Shara.setActiveParser(name);
        chrome.extension.sendRequest({operation: 'relodeParser'}, function (response) {
            log(response, 'warn');
            Shara.relode();
        });
    },
    removeHandler: function (name) {
        if (!Mixins.validate(name, 'isEmpty')) {
            alert('Не передано имя парсера!');
            return;
        }
        if (name === "default") {
            alert('Парсер по умолчанию не может быть удален!');
            return;
        }
        var newParsers = [];
        st.get('parsers').then(function (parsers) {
            if (parsers) {
                var ln = parsers.length, delIndex;
                for (var i = 0; i < ln; i++) {
                    var loc = parsers[i];
                    if (loc.name !== name) {
                        newParsers.push(loc);
                    } else {
                        if (loc.active) {
                            delIndex = i;
                        }
                    }
                }
                if (delIndex) {
                    newParsers[delIndex] ? newParsers[delIndex].active = true : newParsers[0].active = true;
                }
                st.set('parsers', newParsers);
                chrome.extension.sendRequest({operation: 'relodeParser'}, function (response) {
                    log(response, 'warn');
                    Shara.relode();
                });
            }
        });
    },
    render: function () {
        var parsers = {}, i = 0;
        this.state.parsers.forEach(function (parser) {
            parsers['parser_' + i] = <tr key={i} className={parser.active ? "active" : ""}>
                <td>
                    <input onClick={this.changeParserHandler.bind(this, parser.name)} type="radio" name="active" defaultChecked={parser.active ? true : false} />
                </td>
                <td>
                    <b>{parser.name}</b>
                    <input type="hidden" className="data" name="name" defaultValue={parser.name} />
                </td>
                <td>
                    <span>{parser.url}</span>
                    <input type="text" className="data" data-need="true" name="url" defaultValue={parser.url} />
                </td>
                <td>
                    <span>{parser.method}</span>
                    <select name="method">
                        <option value="GET">GET</option>
                        <option value="POST">POST</option>
                    </select>
                </td>
                <td>
                    <span>{parser.domenParam}</span>
                    <input type="text" name="domenParam" className="data" defaultValue={parser.domenParam} />
                </td>
                <td>
                    <span>{parser.params}</span>
                    <textarea name="params" defaultValue={parser.params}></textarea>
                </td>
                <td>
                    <i onClick={this.editHandler} className="fa fa-pencil-square-o fa-3x edit"></i>
                    <i onClick={this.saveHandler} className="fa fa-floppy-o fa-3x save" title="Сохранить изменения"></i>
                    <i onClick={this.cancelHandler} className="fa fa-times fa-3x cancel" title="Отменить"></i>
                </td>
                <td>
                    <i onClick={this.removeHandler.bind(this, parser.name)} className="fa fa-times fa-2x remove" title="Удалить парсер"></i>
                </td>
            </tr>;
            i++;
        }.bind(this));
        return (
            <table id="parserTable" className="parserTable lister">
                <tr>
                    <th></th>
                    <th>Метка</th>
                    <th>URL запроса</th>
                    <th>Метод</th>
                    <th>Параметр для имени домена</th>
                    <th>Параметры запроса</th>
                    <th></th>
                    <th></th>
                </tr>
                {parsers}
            </table>
        );
    }
});

module.exports = SourceList;