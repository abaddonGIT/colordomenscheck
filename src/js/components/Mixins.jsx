"use strict";
var React = require('react');

var Mixins = {
    validate: function (text, type) {
        switch (type) {
            case 'isDomen':
                return /^([\a-zа-яё0-9\.-]+)\.([a-zа-яё\.]{2,6})([\/\w \.-]*)*\/?$/i.test(text);
                break;
            case 'isEmpty':
                return /^[a-zа-яё0-9\?\&\=]/i.test(text);
                break;
        }
    }
};

module.exports = Mixins;