var React = require('react'),
    Shara = require('../venders/sharedMethods.js'),
    config = require('../config'),
    log = require('../venders/log'),
    st = require('../venders/storage'),
    AddDomainForm = require('./options/AddDomainForm.jsx'),
    ExtensionSettings = require('./options/ExtensionSettings.jsx'),
    DomainList = require('./options/DomainList.jsx');


var FileField = React.createClass({
    getDefaultProps: function () {
        return {
            forDelete: {}
        }
    },
    getInitialState: function () {
        return config.optionPage;
    },
    handleChange: function (e) {
        var file = e.target.files[0], Reader = new FileReader();

        if (!file) {
            this.setState({'message': "Внимание пустой файл!!!"});
            return;
        }

        if (file.name.indexOf('.csv') === -1) {
            this.setState({'message': "Ну написано же, только CSV!!!"});
            return;
        }
        Reader.onload = function (event) {
            var content = Reader.result;
            if (content.length) {
                var contentArr = content.split("\n"), domains = Shara.clearArray(contentArr);
                this.setState({'domains': domains});
                //Сохраняем в хранилище
                st.set('domains', domains);
                chrome.extension.sendRequest({operation: "file", domenlist: domains}, function (response) {
                    log(response);
                });
                this.setState({'message': " "});
            } else {
                this.setState({'message': "Внимание пустой файл!!!"});
            }
        }.bind(this);
        Reader.readAsText(file);
    },
    componentWillMount: function () {
        st.get('domains').then(function (domains) {
            if (domains) {
                if (domains) {
                    this.setState({'domains': domains, 'message': ""});
                    if (!domains.length) {
                        this.setState({'message': "Пока тут шаром покати!!!"});
                    }
                } else {
                    this.setState({'message': "Пока тут шаром покати!!!"});
                }
            } else {
                this.setState({'message': "Пока тут шаром покати!!!"});
            }
        }.bind(this));
        st.get('config').then(function (config) {
            if (config) {
                this.setState({config: config});
            } else {
                this.setState({config: this.state.config});
                st.set('config', this.state.config);
            }
        }.bind(this));
        st.get('loading').then(function (loader) {
            if (loader) {
                this.refs.loader.getDOMNode().classList.add('show');
            }
        }.bind(this));
        chrome.storage.onChanged.addListener(function (changes, namespace) {
            var state = changes.loading;
            if (!state) {
                return;
            }
            st.get('domains').then(function (domains) {
                !domains || this.setState({'domains': domains, 'message': ""});
            }.bind(this));
            if (state.newValue) {
                this.refs.loader.getDOMNode().classList.add('show');
            } else {
                this.refs.loader.getDOMNode().classList.remove('show');
            }
        }.bind(this));
    },
    setConfigHandler: function (newConfig) {
        var config = {};
        for (var item in newConfig) {
            var elem = newConfig[item].getDOMNode(), value;
            if (elem.type === 'checkbox') {
                value = elem.checked;
            } else {
                value = elem.value.trim();
            }
            config[elem.name] = value;
        }
        this.setState({config: config});
        st.set('config', config);
    },
    addDomenHandler: function (domen) {
        var domains = this.state.domains;
        this.setState({lock: true});
        if (!Shara.inArray(domains, domen)) {
            chrome.extension.sendRequest({operation: "addDomen", domen: domen}, function (response) {
                domains.push({name: domen});
                this.setState({domains: domains, lock: false, 'message': ""});
            }.bind(this));
        } else {
            this.setState({lock: false});
            alert('Такой домен уже есть в списке!');
        }
    },
    itemHandler: function (listElem) {
        var button = this.refs.delete.getDOMNode(), list = listElem.getDOMNode(), checkeds = list.querySelectorAll('input[type=checkbox]:checked'), ln = checkeds.length;
        this.props.forDelete = {};

        if (checkeds.length) {
            button.classList.remove('hide');
            button.classList.add('inline');
        } else {
            button.classList.remove('inline');
            button.classList.add('hide');
        }

        while (ln--) {
            var key = checkeds[ln].getAttribute('data-key');
            this.props.forDelete[key] = 'delete';
        }
    },
    removeDomains: function (e) {
        var removed = this.props.forDelete, button = this.refs.delete.getDOMNode();
        chrome.extension.sendRequest({operation: "removeDomen", removed: removed}, function (domains) {
            if (!domains.length) {
                this.setState({'domains': domains, 'message': "Пока тут шаром покати!!!"});
            } else {
                this.setState({'domains': domains});
            }
            button.classList.remove('inline');
            button.classList.add('hide');
        }.bind(this));
        e.preventDefault();
    },
    render: function () {
        return (
            <div>
                <div className="col-md-6">
                    <form action="#" method="post" className="clearfix">
                        <fieldset>
                            <legend>Список доменов. (CSV).</legend>
                            <label>
                                <input type="file" name="file" onChange={this.handleChange} />
                            </label>
                        </fieldset>
                    </form>
                    <ExtensionSettings config={this.state.config} setConfigHandler={this.setConfigHandler} />
                </div>
                <div className="col-md-6">
                    <AddDomainForm domains={this.state.domains} adddomen={this.addDomenHandler} lock={this.state.lock} />
                    <legend>
                    Список доменов:
                        <button ref="delete" onClick={this.removeDomains} className="hide delete fa fa-times fa-2x" title="Удалить отмеченные домены"></button>
                    </legend>
                    <div className="loader" ref="loader">
                        <span className="lt"></span>
                        <span className="rt"></span>
                        <span className="rb"></span>
                        <span className="lb"></span>
                    </div>
                    <DomainList domains={this.state.domains} itemHandler={this.itemHandler} />
                    <p className="message">{this.state.message}</p>
                </div>
            </div>
            );
    }
});

module.exports = FileField;