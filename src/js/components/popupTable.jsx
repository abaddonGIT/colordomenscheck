var React = require('react'),
    Shara = require('../venders/sharedMethods.js'),
    st = require('../venders/storage');

var TableTr = React.createClass({
    contextMenu: function (e) {
        var target = e.currentTarget.id;
        this.props.open(target);
        e.preventDefault();
    },
    render: function () {
        var tr = {}, i = 0;
        this.props.out.forEach(function (result) {
            var config = this.props.config, style = {}, cl;

            if (result.difference <= config.dangerosLevel) {
                style.backgroundColor = config.dengerosColor;
            } else if (result.difference <= config.warnLevel) {
                style.backgroundColor = config.warnColor;
            } else {
                style.backgroundColor = config.goodColor;
            }
            if (!result.createDate) {
                cl = 'not-found';
            }
            tr['result-' + i] = <tr style={style} className={cl} onClick={this.contextMenu} id={result.name}>
                <td>{result.name}</td>
                <td>{result.createDate}</td>
                <td>{result.paidTill}</td>
                <td>{result.freeDate}</td>
                <td>{result.difference} дн.</td>
            </tr>;
            i++;
        }.bind(this));

        return (
            <table>
                <tr>
                    <th>Домен</th>
                    <th>Дата регистрации</th>
                    <th>Срок оплаты</th>
                    <th>Срок освобождения</th>
                    <th>Осталось</th>
                </tr>
                {tr}
            </table>
            );
    }
});

var PopupTable = React.createClass({
    getInitialState: function () {
        return {
            out: [],
            config: {},
            currName: "",
            whois: ""
        };
    },
    setNewValue: function () {
        var ident = Shara.getIdentificator();
        st.get('domains').then(function (domains) {
            if (domains) {
                this.setState({'out': domains});
            }
        }.bind(this));
    },
    componentWillMount: function () {
        chrome.storage.onChanged.addListener(function (changes, namespace) {
            for (var key in changes) {
                if (key === 'domains') {
                    this.setNewValue();
                }
            }
        }.bind(this));

        st.get('config').then(function (config) {
            this.setState({config: config});
        }.bind(this));

        this.setNewValue();

        st.get('loading').then(function (loader) {
            if (loader) {
                this.refs.loader.getDOMNode().classList.add('show');
            }
        }.bind(this));

        chrome.storage.onChanged.addListener(function (changes, namespace) {
            var state = changes.loading;
            if (!state) {
                return;
            }
            if (state.newValue) {
                this.refs.loader.getDOMNode().classList.add('show');
            } else {
                this.refs.loader.getDOMNode().classList.remove('show');
            }
        }.bind(this));
    },
    openPanelHandler: function (name) {
        var panel = this.refs.panel.getDOMNode();
        panel.classList.remove('hide');
        panel.classList.add('show');
        this.setState({currName: name, whois: "http://who.is/whois/" + name});
    },
    render: function () {
        return (
            <div>
                <div className="loader" ref="loader">
                    <span className="lt"></span>
                    <span className="rt"></span>
                    <span className="rb"></span>
                    <span className="lb"></span>
                </div>
                <TableTr out={this.state.out} config={this.state.config} open={this.openPanelHandler} />
                <div ref="panel" id="dop-action" className="hide">
                    <header>
                    {this.state.currName}
                    </header>
                    <nav>
                        <a href={this.state.whois} target="_blank">Посмотреть whois</a>
                    </nav>
                </div>
            </div>
            );
    }
});

module.exports = PopupTable;