"use strict";
var React = require('react'),
    st = require('../venders/storage'),
    log = require('../venders/log'),
    config = require('../config'),
    Mixins = require('./mixins.jsx'),
    Shara = require('../venders/sharedMethods.js'),
    SourceList = require('./parsers/SourceList.jsx'),
    AddParser = require('./parsers/AddParser.jsx');

var CodPlace = React.createClass({
    getDefaultProps: function () {
        return {
            activeParser: {}
        }
    },
    getInitialState: function () {
        return config.codPlace;
    },
    codChangeHandler: function (cod) {
        this.props.activeParser.cod = JSON.stringify(cod);
        this.setState({cod: cod});
        Shara.saveChangeActiveParser(cod);
    },
    sendHandler: function (e) {
        var func = JSON.stringify(this.state.cod);
        chrome.extension.sendRequest({func: func, operation: 'test'}, function (response) {

        }.bind(this));
        e.preventDefault();
    },
    editorInit: function () {
        var editor = ace.edit("editor");
        editor.setTheme("ace/theme/twilight");
        editor.getSession().setMode("ace/mode/javascript");
        editor.getSession().on('change', function () {
            var value = editor.getSession().getValue();
            this.refs.cod.getDOMNode().value = value;
            this.codChangeHandler(value);
        }.bind(this));
    },
    componentWillMount: function () {
        Shara.getActiveParser().then(function (parser) {
            var cod;
            this.props.activeParser = parser;
            try {
                cod = JSON.parse(parser.cod);
            } catch (e) {
                cod = parser.cod;
            }
            this.setState({cod: cod, ace: cod});
            this.refs.cod.getDOMNode().value = cod;
            this.editorInit();
        }.bind(this));
    },
    setDefaultHandler: function (e) {
        Shara.setActiveParser('def');
        chrome.extension.sendRequest({operation: 'relodeParser'}, function (response) {
            log(response, 'warn');
            Shara.relode();
        });
        e.preventDefault();
    },
    setNewParserHandler: function (e) {
        chrome.extension.sendRequest({operation: 'relodeParser'}, function (response) {
            log(response, 'warn');
        });
        e.preventDefault();
    },
    refreshHandler: function (e) {
        chrome.extension.sendRequest({operation: 'refresh'}, function (response) {
            log(response, 'warn');
        });
        e.preventDefault();
    },
    unloadingHandler: function (e) {
        st.get('parsers').then(function (parsers) {
            var text = JSON.stringify(parsers), link = document.createElement('a');
            link.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(text));
            link.setAttribute('download', 'parsers.txt');
            link.click();
        });
        e.preventDefault();
    },
    loadParsersChangler: function (e) {
        var file = e.target.files[0], reader = new FileReader();

        if (!file) {
            alert('Не выбран файл!!!');
            return;
        }

        reader.onloadend = function () {
            var content = this.result;
            try {
                content = JSON.parse(content);
                st.set(parsers, content);
                chrome.extension.sendRequest({operation: 'relodeParser'}, function (response) {
                    log(response, 'warn');
                    Shara.relode();
                });
            } catch (e) {
                alert("при разборе файла произошла ошибка!");
            }
        };

        reader.onerror = function () {
            alert('Файл не может быть прочитан!');
        }

        reader.readAsText(file);
    },
    render: function () {
        return (
            <div id="editor-place">
                <div className="tcenter">
                    <button className="btn default" onClick={this.setDefaultHandler}>По умолчанию</button>
                    <button className="btn refresh" onClick={this.refreshHandler}>Сброс кэша</button>
                    <button className="btn unloading" onClick={this.unloadingHandler}>Сделать копию</button>
                    <button className="btn accept" onClick={this.setNewParserHandler}>Применить</button>
                    <input type="file" name="parsersFile" onChange={this.loadParsersChangler} />
                </div>
                <div className="clear"></div>
                <AddParser />
                <form>
                    <fieldset>
                        <SourceList />
                    </fieldset>
                </form>
                <div id="wrap-editor">
                    <div id="editor" ref="editor">{this.state.ace}</div>
                </div>
                <textarea name="code" ref="cod" defaultValue={this.state.cod}></textarea>
            </div>
        );
    }
});

module.exports = CodPlace;