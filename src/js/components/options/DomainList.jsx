var React = require('react'), st = require('../../venders/storage');

var ListItem = React.createClass({
    componentWillMount: function () {

    },
    getDefaultProps: function () {
        return {
            click: {}
        }
    },
    markElement: function (e) {
        var target = e.target;
        this.props.itemHandler(this.props.list);
    },
    domenUbdateHandler: function (name, e) {
        var that = this, el = e.target;
        st.get('lock').then(function (lock) {
            if (!lock) {
                st.set('lock', true);
                chrome.extension.sendRequest({operation: "updateDomen", name: name}, function (response) {
                    console.log('update');
                }.bind(this));
            }
        });
    },
    render: function () {
        return (
            <li>
                <input type="checkbox" data-key={this.props.item.name} onClick={this.markElement} />
                {this.props.item.name}
                <i className="fa fa-refresh fa-2x right refresh" onClick={this.domenUbdateHandler.bind(this, this.props.item.name)} title="Обновить данные"></i>
            </li>
        );
    }
});

var DomainList = React.createClass({
    render: function () {
        var i = 0, li = this.props.domains.map(function (domain) {
            i++;
            return <ListItem item={domain} key={i} list={this.refs.domensList} itemHandler={this.props.itemHandler} />;
        }.bind(this));
        return (
            <div id="list">
                <ol ref="domensList">
                {li}
                </ol>
            </div>
        );
    }
});

module.exports = DomainList;