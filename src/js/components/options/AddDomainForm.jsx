var React = require('react'),
    Mixins = require('../mixins.jsx');

var AddDomainForm = React.createClass({
    mixins: [Mixins],
    addDomenHandler: function (e) {
        if (!this.props.lock) {
            var value = this.refs.add.getDOMNode().value.trim();
            if (!Mixins.validate('isDomen', value)) {
                value = value.replace(/\s/g, "");
                this.props.adddomen(value);
                this.refs.add.getDOMNode().value = "";
            } else {
                alert('Неправельный формат доменного имени!');
            }
        }
        e.preventDefault();
    },
    render: function () {
        return (
            <form id="addForm">
                <fieldset>
                    <legend>Добавить домен</legend>
                    <div className="col-md-9 row">
                        <input type="text" name="add" ref="add" placeholder="Доменное имя" />
                    </div>
                    <div className="col-md-3">
                        <button onClick={this.addDomenHandler}>
                            <i className="fa fa-plus-circle fa-3x"></i>
                        </button>
                    </div>
                </fieldset>
            </form>
            );
    }
});

module.exports = AddDomainForm;