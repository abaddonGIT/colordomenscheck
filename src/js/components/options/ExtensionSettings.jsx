var React = require('react');

var ExtensionSettings = React.createClass({
    savingSettingsHandler: function (e) {
        var refs = this.refs;
        this.props.setConfigHandler(refs);
        if (e.target.type !== 'checkbox') {
            e.preventDefault();
        }
    },
    render: function () {
        return (
            <form>
                <fieldset>
                    <legend>Настройки</legend>
                    <label title="Включает текстовые оповещения для отладки">Режим отладки
                        <input type="checkbox" name="debag" checked={this.props.config.debag} onChange={this.savingSettingsHandler} ref="debag" />
                    </label>
                    <label>Показывать уведомления
                        <input type="checkbox" name="notification" checked={this.props.config.notification} onChange={this.savingSettingsHandler} ref="notification" />
                    </label>
                    <label>За сколько дней ставить статус "предупреждение" ({this.props.config.warnLevel})
                        <input type="range" name="warnLevel" min="1" max="100" step="1" ref="warnLevel" onChange={this.savingSettingsHandler} value={this.props.config.warnLevel} />
                    </label>
                    <label>За сколько дней ставить статус "Опасность" ({this.props.config.dangerosLevel})
                        <input type="range" name="dangerosLevel" min="1" max="100" step="1" onChange={this.savingSettingsHandler} ref="dangerosLevel" value={this.props.config.dangerosLevel} />
                    </label>
                    <label className="col-md-4">Цвет статуса "предупреждение"
                        <input type="color" name="warnColor" value={this.props.config.warnColor} onChange={this.savingSettingsHandler} ref="warnColor" />
                    </label>
                    <label className="col-md-4">Цвет статуса "Опасность"
                        <input type="color" name="dengerosColor" value={this.props.config.dengerosColor} onChange={this.savingSettingsHandler} ref="dengerosColor" />
                    </label>
                    <label className="col-md-4">Цвет статуса "Норма"
                        <input type="color" name="goodColor" value={this.props.config.goodColor} onChange={this.savingSettingsHandler} ref="goodColor" />
                    </label>
                </fieldset>
            </form>
            );
    }
});

module.exports = ExtensionSettings;